<?php

/**
 * Вспомогательные функции
 */

/**
 * Переводит snake-case в CamelCase
 * @param string $url
 * @return string
 */
function urlToClassName($url)
{
    return preg_replace_callback("/(?:^|-)([a-z])/", function($matches) {
        return strtoupper($matches[1]);
    }, $url);
}

function query()
{
    return App::instance()->database->createQueryBuilder();
}

function getPSRFileName($className)
{
    $className = ltrim($className, '\\');
    $fileName = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    return ROOT . '/app/' . $fileName;
}