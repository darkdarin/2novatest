<?php namespace Controller;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegisterController extends BaseController
{
    /**
     * Страница регистрации
     * @return string
     */
    public function indexAction()
    {
        $this->title = 'Регистрация';

        $form = $this->getRegisterForm();

        // если форма была отправлена
        if ($form->isSubmitted()) {
            $data = $form->getData();

            if ($data['password'] != $data['password_confirm']) {
                $form->get('password_confirm')->addError(new FormError('Подтверждение пароля не совпадает с паролем'));
            }

            if ($form->isValid()) {
                // ищем указанный email в БД
                $result = query()->select('*')
                    ->from('users')
                    ->where('email = ?')
                    ->setParameter(0, $data['email'])
                    ->execute();

                $user = $result->fetch(\PDO::FETCH_ASSOC);

                // если есть - генерируем ошибку
                if ($user) {
                    $form->addError(new FormError('Пользователь с указанным email уже зарегистрирован'));
                } else {
                    // добавляем пользователя в БД
                    $result = query()->insert('users')
                        ->values([
                            'name' => ':name',
                            'password' => ':password',
                            'email' => ':email'
                        ])
                        ->setParameters([
                            'name' => $data['name'],
                            'password' => password_hash($data['password'], PASSWORD_BCRYPT),
                            'email' => $data['email']
                        ])
                        ->execute();

                    if (!$result) {
                        $form->addError(new FormError('Во время сохранения произошла ошибка (('));
                    }
                }

                if (!count($form->getErrors())) {
                    $this->session->set('user', $user);

                    return $this->redirect('/');
                }
            }
        }

        $this->form = $form->createView();

        return $this->render();
    }

    /**
     * Возвращает сконфигурированную форму регистрации
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function getRegisterForm()
    {
        // валидаторы
        $emailValidation = new Email([
            'message' => 'Укажите правильный email-адрес'
        ]);
        $passwordValidation = [
            new NotBlank([
                'message' => 'Укажите свой пароль'
            ]),
            new Length([
                'minMessage' => 'Длина пароля должна быть от 4 до 30 символов',
                'maxMessage' => 'Длина пароля должна быть от 4 до 30 символов',
                'min' => 4,
                'max' => 30
            ])
        ];
        $nameValidation = new NotBlank([
            'message' => 'Укажите свое имя'
        ]);

        // настройка формы регистрации
        $form = $this->createFormBuilder()
            ->add('name', TextType::class, [
                'label' => 'Имя',
                'constraints' => $nameValidation
            ])
            ->add('email', EmailType::class, [
                'constraints' => $emailValidation
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Пароль',
                'constraints' => $passwordValidation
            ])
            ->add('password_confirm', PasswordType::class, [
                'label' => 'Подтверждение пароля',
            ])
            ->getForm();

        $form->handleRequest(\App::instance()->request);

        return $form;
    }
}