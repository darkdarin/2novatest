<?php namespace Controller;

use App;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRenderer;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;
use Symfony\Component\Security\Csrf\TokenStorage\SessionTokenStorage;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validation;
use Twig_Environment;
use Twig_Loader_Filesystem;

class BaseController
{
    /** @var string Расширение фйалов-шаблонов по умолчанию */
    public $partialExtension = 'htm';
    /** @var string Макет */
    public $layout = 'layout';
    /** @var string Шаблон для форм */
    public $defaultFormTheme = 'bootstrap_3_layout';

    protected $twig = null;
    protected $formFactory = null;
    protected $csrfManager = null;
    protected $session = null;

    protected $data = [];

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->session = new Session();
        $this->twig = $this->initializeTwig();

        $this->page = '';
        $this->title = 'Default Page';
    }

    /**
     * Метод инициализирует Twig, а также подключает необходимые для него расширения
     */
    protected function initializeTwig()
    {
        // путь к шаблонам для форм
        $appVariableReflection = new \ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
        $vendorTwigBridgeDir = dirname($appVariableReflection->getFileName());

        //нинциализация twig
        $twig = new Twig_Environment(new Twig_Loader_Filesystem([
            ROOT . '/app/views',
            $vendorTwigBridgeDir . '/Resources/views/Form',
        ]), [
            'cache' => ROOT . '/cache/views',
            'debug' => true
        ]);

        // csrf для форм
        $csrfGenerator = new UriSafeTokenGenerator();
        $csrfStorage = new SessionTokenStorage($this->session);
        $this->csrfManager = $csrfManager = new CsrfTokenManager($csrfGenerator, $csrfStorage);

        $formEngine = new TwigRendererEngine([
                $this->defaultFormTheme . '.html.twig'
            ], $twig);

        $twig->addRuntimeLoader(new \Twig_FactoryRuntimeLoader(array(
            TwigRenderer::class => function () use ($formEngine, $csrfManager) {
                return new TwigRenderer($formEngine, $csrfManager);
            },
        )));

        // добавляем в twig расширения
        $twig->addExtension(new FormExtension());
        $twig->addExtension(new TranslationExtension(new Translator('ru')));

        return $twig;
    }

    /** Магические методы для установки шаблонных переменных */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return $this->data[$name];
    }

    /**
     * Рендерит указанный шаблон, вставляя содержимое в макет
     * Если шаблон не указан - будет использовать шаблон имя-контроллера/имя-метода.htm
     * Шаблоны страниц лежат в папке app/views/pages
     * @param string $partialName
     * @return string
     */
    public function render($partialName = null)
    {
        if ($partialName == null) {
            $partialName = App::instance()->controller . DIRECTORY_SEPARATOR . App::instance()->method;
        }

        $this->page = $this->twig->render('pages/' . $partialName . '.' . $this->partialExtension, $this->data);

        return $this->renderLayout();
    }

    /**
     * Рендерит указанный макет
     * Если имя макета не передано - использует имя макета по умолчанию
     * @param string $layout
     * @return string
     */
    public function renderLayout($layout = null)
    {
        return $this->twig->render($layout ? $layout : $this->layout . '.' . $this->partialExtension, $this->data);
    }

    /**
     * Метод 404
     * @return string
     */
    public function notFoundAction()
    {
        $html = $this->render('404');

        $response = new Response($html);
        $response->setStatusCode(404);

        return $response->send();
    }

    /**
     * @return \Symfony\Component\Form\FormBuilderInterface
     */
    public function createFormBuilder()
    {
        if ($this->formFactory == null) {
            $this->formFactory = Forms::createFormFactoryBuilder()
                ->addExtension(new HttpFoundationExtension())
                ->addExtension(new CsrfExtension($this->csrfManager))
                ->addExtension(new ValidatorExtension(Validation::createValidator()))
                ->getFormFactory();
        }

        return $this->formFactory->createBuilder();
    }

    /**
     * Редирект на указанный URL
     * @param string $url
     * @return Response
     */
    public function redirect($url)
    {
        $response = new RedirectResponse($url);
        $response->prepare(App::instance()->request);

        return $response->send();
    }
}