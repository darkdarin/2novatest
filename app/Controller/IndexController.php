<?php namespace Controller;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class IndexController extends BaseController
{
    /**
     * Главная страница
     * Если не авторизован - редирект на страницу авторизации
     * @return string
     */
    public function indexAction()
    {
        if (!$user = $this->session->get('user')) {
            return $this->redirect('/index/auth');
        }

        $this->user = $user;
        $this->title = 'Привет, ' . $user['name'];

        return $this->render();
    }

    /**
     * Страница авторизации
     * @return string
     */
    public function authAction()
    {
        $this->title = 'Авторизация';

        $form = $this->getLoginForm();

        // если форма была отправлена
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // находим пользователя в БД
            $result = query()->select('*')
                ->from('users')
                ->where('email = ?')
                ->setParameter(0, $data['email'])
                ->execute();

            $user = $result->fetch(\PDO::FETCH_ASSOC);

            // если не нашелся, или пароль не совпадает - генерируем ошибки
            if (!$user) {
                $form->addError(new FormError('Пользователь с указанными данными не найден'));
            } elseif (!password_verify($data['password'], $user['password'])) {
                $form->addError(new FormError('Пользователь с указанными данными не найден'));
            }

            if (!count($form->getErrors())) {
                $this->session->set('user', $user);

                return $this->redirect('/');
            }
        }

        $this->form = $form->createView();

        return $this->render();
    }

    /**
     * Выход пользователя из аккаунта
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoutAction()
    {
        $this->session->remove('user');

        return $this->redirect('/');
    }

    /**
     * Возвращает сконфигурированную форму входа
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function getLoginForm()
    {
        // валидаторы
        $emailValidation = new Email([
            'message' => 'Укажите правильный email-адрес'
        ]);
        $passwordValidation = [
            new NotBlank([
                'message' => 'Укажите свой пароль'
            ]),
            new Length([
                'minMessage' => 'Длина пароля должна быть от 4 до 30 символов',
                'maxMessage' => 'Длина пароля должна быть от 4 до 30 символов',
                'min' => 4,
                'max' => 30
            ])
        ];

        // настройка формы авторизации
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class, [
                'constraints' => $emailValidation
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Пароль',
                'constraints' => $passwordValidation
            ])
            ->getForm();

        $form->handleRequest(\App::instance()->request);

        return $form;
    }

}