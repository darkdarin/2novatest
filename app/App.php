<?php

/**
 * Class App
 * Класс приложения
 * Инициализирует основные классы, использует свой роутинг
 * Инициирует подключение к БД
 */
class App
{
    protected static $instance = null;

    /** @var Doctrine\DBAL\Connection Подключение к БД */
    public $database = null;
    /** @var \Symfony\Component\HttpFoundation\Request */
    public $request = null;
    /** @var array Конфигурация */
    public $config = null;

    /** @var string Имя контроллера в snake-case */
    public $controller = null;
    /** @var string Имя метода в snake-case */
    public $method = null;

    /** @var string Класс контроллера */
    public $routeController = null;
    /** @var string Имя метода */
    public $routeMethod = null;
    /** @var array Передаваемые в метод контроллера параметры */
    public $routeParams = [];

    /**
     * Возвращает экземпляр приложения
     * Если приложение еще не было создано - генерируется исключение
     * @return App
     * @throws Exception
     */
    public static function instance()
    {
        if (!self::$instance) {
            throw new Exception('Попытка использовать класс App до его инициализации');
        }

        return self::$instance;
    }

    public function __construct($config)
    {
        $this->config = $config;
        $this->request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

        $this->database = $this->getDatabaseConnection();
        $this->getRoute();

        self::$instance = $this;
    }

    /**
     * Осуществляет подключение к БД
     * @return \Doctrine\DBAL\Connection
     */
    public function getDatabaseConnection()
    {
        $config = new \Doctrine\DBAL\Configuration();
        return \Doctrine\DBAL\DriverManager::getConnection($this->config['database'], $config);
    }

    /**
     * Возвращает информацию о контроллере, методе и передаваемых параметрах по URL
     */
    public function getRoute()
    {
        $path = trim($this->request->getPathInfo(), '/');

        $pathParts = explode('/', $path);

        $this->controller = strlen($pathParts[0]) ? $pathParts[0] : 'index';
        $this->method = isset($pathParts[1]) && strlen($pathParts[1]) ? $pathParts[1] : 'index';

        $this->routeController = 'Controller\\' . urlToClassName($this->controller) . 'Controller';
        $this->routeMethod = lcfirst($this->method) . 'Action';

        // если нужного контроллера нет - генерируем 404
        if (!class_exists($this->routeController)) {
            $this->routeController = 'Controller\\BaseController';
            $this->routeMethod = 'notFoundAction';
        }

        if (count($pathParts) > 2) {
            $this->routeParams = array_slice($pathParts, 2);
        }
    }

    /**
     * Запускаем нужный метод контроллера
     */
    public function execute()
    {
        // Получаем ответ от контроллера
        $controller = new $this->routeController();
        if (method_exists($controller, $this->routeMethod)) {
            $responseBody = call_user_func_array([$controller, $this->routeMethod], $this->routeParams);
        } else {
            $responseBody = call_user_func([$controller, 'notFoundAction']);
        }

        // Отправляем ответ
        $response = new \Symfony\Component\HttpFoundation\Response($responseBody);
        $response->send();
    }
}