#Примечания к установке
* необходимо выставить права на запись на папку /cache/ пользователю, от имени которого работает PHP
* composer update
* для правильного роутинга в конфигурации nginx должен быть раздел переадресации запросов:


    location / {
        try_files $uri /index.php$is_args$args;
    }
    
* выполнить запрос MySQL


    CREATE TABLE `users` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `email` varchar(200) NOT NULL,
      `password` varchar(100) NOT NULL,
      `name` varchar(100) NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `users_email_uindex` (`email`)
    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
    
* скопировать содержимое config.origin.php в config.php и актуализировать настройки в нем