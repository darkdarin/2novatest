<?php

define('ROOT', __DIR__);

// Config
$config = require ROOT . '/config.php';
// Helpers Functions
require ROOT . '/helpers.php';

// Composer Autoload
require ROOT . '/vendor/autoload.php';

// Class Autoload
spl_autoload_register(function ($className) {
    $fileName = getPSRFileName($className);

    if (file_exists($fileName)) {
        require getPSRFileName($className);
    }
});

// Создаем экземпляр приложения и запускаем его
$app = new App($config);
$app->execute();